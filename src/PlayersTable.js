import { useEffect, useState } from 'react'

export const PlayersTable = () => {
  const DIRECTION = "DIRECTION";
  const ITEMS = "ITEMS";
  const DIRECTIONS = {
    ASC: "ASC",
    DESC: "DESC"
  };

  const dataValue = localStorage.getItem(ITEMS);
  const directionValue = localStorage.getItem(DIRECTION);
  const [direction, setDirection] = useState(directionValue ? directionValue : DIRECTIONS.DESC);
  const [playersData, setPlayersData] = useState(dataValue ? JSON.parse(dataValue) : []);
  const [listening, setListening] = useState(false);

  useEffect(() => {
    if (!listening) {
      const SSESource = new EventSource('http://localhost:5000/');

      SSESource.onmessage = (event) => {
        setPlayersData((prev) => {
          const updateData = prev.concat(JSON.parse(event.data));
          localStorage.setItem(ITEMS, JSON.stringify(updateData));
          return updateData;
        });
      };
      SSESource.onerror = (error) => {
        console.error(error);
        SSESource.close();
      };
      setListening(true);
    }
  }, [listening, playersData]);

  const renderPlayersData = () => {
    if (!playersData) return null;

    const sorted = Array.from(playersData).sort((a, b) => {
      return direction === DIRECTIONS.DESC ? a.score - b.score : b.score - a.score;
    });

    return sorted.map((player, index) => {
      return (
        <tr key={index}>
          <th>{player.player}</th>
          <th>{player.score}</th>
        </tr>
      )
    })
  }

  const handleDirectionClick = (direction) => {
    localStorage.setItem(DIRECTION, direction);
    setDirection(direction);
  }

  return (
    <>
      <table>
        <thead>
          <tr>
            <th>Player name</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          { renderPlayersData() }
        </tbody>
      </table>
      <button onClick={() => handleDirectionClick(DIRECTIONS.DESC)}>DESC</button>
      <button onClick={() => handleDirectionClick(DIRECTIONS.ASC)}>ASC</button>
    </>
  )
}
