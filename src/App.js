import logo from './logo.svg';
import './App.css';
import { PlayersTable } from './PlayersTable';

function App() {
  return (
    <div className="App">
      <PlayersTable />
    </div>
  );
}

export default App;
